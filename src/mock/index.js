import Mock from 'mockjs'
import { getUserInf } from './response/user'

Mock.mock(/\/getUserInfo/,'post',getUserInf)

export default Mock