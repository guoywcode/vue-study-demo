import Mock from 'mockjs'

export const getUserInf = (options) => {
  const template = {
    'userId': 1,
    'name|2-3': 's',
    'nameTo|3': 'a'

  }

  return Mock.mock(template)
}