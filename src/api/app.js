
import request from '@/utils/request'


export const getAppName = () => {
  return new Promise((resolve, reject) => {
    const err = null
    setTimeout(() => {
      if (!err) resolve({ code: 200, info: { appName: 'apiAppName' } })
      else reject(err)
    })
  })
}

export const getUserInfo = ({userId})=>{
  return new request().request({
    url: '/index/getUserInfo',
    method: 'post',
    data: {
      userId
    }
  })
}