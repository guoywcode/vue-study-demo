import Home from '../views/Home.vue'

export const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home,
    props: route => ({
      food: route.query.food
    }),
    beforeEnter: (to, from, next) => {
      // if (from.name === 'About')
      //   alert("来自于About页")
      // else
      //   alert('来自于其他页面')

      next()
    }
  },
  {
    path: '/login',
    name: 'Login',
    component: () => import('@/views/Login')
  },
  {
    path: '/about',
    alias: '/about_page',
    name: 'About',
    meta: {
      tile: '关于'
    },
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue'),
    props: {
      name: 'hello about!!'
    }
  },
  {
    path: '/argu/:name',
    name: 'Argu',
    component: () => import('@/views/Argu'),
    props: true
  },
  {
    path: '/parent',
    name: 'Parent',
    component: () => import('@/views/Parent'),
    children: [{
      path: 'child',
      component: () => import('@/views/Child')
    }]
  },
  {
    path: '/main_view',
    components: {
      default: () => import('@/views/Child'),
      email: () => import('@/views/Email'),
      tel: () => import('@/views/Tel')
    }
  },
  {
    path: '/main',
    redirect: to => {
      console.log(to)
      return {
        name: 'Home'
      }
    }
  },
  {
    path: '/store',
    name: 'Store',
    component: () => import('@/views/Store')
  }
]