import { getAppName } from '@/api/app'

const actions = {
  // updateAppName({commit}) {
  //   getAppName().then(res => {
  //     const { code, info } = res
  //     console.log(info)
  //     commit('SET_APPNAME', info)
  //   }).catch(err => {
  //     console.log(err)
  //   })
  // }

  async updateAppName({ commit ,state,root}) {
    try {
      const { code, info } = await getAppName()
      commit('SET_APPNAME', info)
    } catch(err){
      console.log(err)
    }
  }

}
export default actions