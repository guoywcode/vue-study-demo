import vue from 'vue'

const mutations ={
  SET_APPNAME:(state,param)=>{
    state.appName = param.appName
    
  },
  SET_APPVERSION:(state,param)=>{
    vue.set(state,param.key,param.value)
  }
}

export default mutations